# JSignPdf

CloudFoxy can now integrate with JSignPdf via a TCP proxy. The use is similar to 
software / Java keystores, e.g.:

`java -jar ./JSignPdf.jar -kp <password> -kst cloudfoxy -ksf localhost:4001 -ka <alias> <PDf file>`

to sign a PDF file with a key `<alias>` and smart card PIN `<password>`, or 

`java -jar ./JSignPdf.jar -lk -kst cloudfoxy -ksf localhost:4001`

to get a list of all aliases available in the CloudFoxy instance.

localhost:4001 - is the host and port of the TCP proxy, which translates SIGN 
command into APDUs for smartcards. The initial implementation is for the Czech
trust provider I.CA.

There are a few steps to get to this stage as the whole integration consists of 
the following components:
1. smartcards - it can be your USB smartcard reader with a card plug to it;
2. FoxyRest - a RESTful API for remote access to smartcards
3. FoxyProxy - a TCP proxy, which translates requests from JSignPdf into the
   language of smartcards (aka APDUs)
4. FoxyProxy configuration - this includes definitions of 3 commands:
   - CHAIN - providing certificate chain for a given user;
   - ALIASES - information linking a name of the signing person to a particular
               smartcard;
   - SIGN - the actual signing request, which can include PIN verification; and
   - creation of an internal dictionary of certificates
5. JSignPdf itself - with our CloudFoxy enhancement (technical details for 
   hardcore readers are in our [pull request](https://github.com/kwart/jsignpdf/pull/24)

Step 4 requires a configuration for a particular type of smartcard, the provided
code shows this for [I.CA](http://ica.cz) - a Czech 
[eIDAS](https://en.wikipedia.org/wiki/EIDAS) trust provider.

## Smartcards

You have two options, which can be combined:
 - standard USB smart-card readers supported by your operating system; or 
 - our CloudFoxy TCP/IP smart-card reader for 120 smartcards.

You can use both or either of them - the selection is done in the FoxyRest
configuration file `global.json`.

## Operating System

It is recommended to increase limits on the maximum number of opened files and a few more
network configuration parameters as applications can create large number of connections

Examples for Linux systems:

### /etc/sysctl.conf

Add the following lines or similar:

```
fs.file-max=100000
net.core.somaxconn=1024
net.ipv4.tcp_max_syn_backlog=10240
net.ipv4.tcp_rfc1337=1
net.ipv4.tcp_fin_timeout=15
net.core.netdev_max_backlog=65536
net.ipv4.tcp_tw_reuse=1
```

### /etc/security/limits.conf

Add the following line:

```
*               -       nofile          100000
```

You should close your current session and open a new one. When you run "ulimit -n",
it should return the new value of 100,000.


## FoxyRest

FoxyRest is a Java application, which extends [GPPro](https://github.com/martinpaljak/GlobalPlatformPro),
which means it is possible to use smartcards, but also managed their applications:
list, delete, install, ...

Detailed instructions for installations are in the [Operations manual](https://gitlab.com/cloudfoxy/FoxyRest/blob/master/OPERATIONS.md)

There a few configuration files:
 - appConfig.yml - the main configuration file for the RESTful API
 - looplog.yml - a logging configuration
 - global.json - a configuration of available smartcars - a simple text file, where
   you simply define names of smart-card readers, or IP addresses of connected 
   [CloudFoxy](https://cloudfoxy.com) cards

## FoxyProxy

This simple proxy is a Python application providing a TCP proxy abastracting the
RESTful API of FoxyRest. It is installed from its 
[PyPI repository](https://pypi.org/project/foxyproxy/). Simply type:

`pip install foxyproxy`

and let the system do what is necessary. The entry point is: `foxyproxy`, 
which you can add to the rc.local for starting when your server reboots (CentOS7):

`sudo -s -- nohup foxyproxy 2>&1 | logger &`

The applications takes 2 optional parameters:

1. -p<port> - the number of the port, where it will be listening for client requests
2. -s<host>:<port> - the address of RESTfulFoxy, e.g., `http://server.cloudfoxy.com:8081`

## FoxyProxy Configuration

You may need to use the proxy as a template and extend it for your own type of smartcards.
It is supposed to be a flexible tool for translating application requests into 
smartcard commands.

We will extend out-of-the-box functionality of the proxy and you're wellcome to
send over your suggestions / use-cases.

## JSignPDF

You can download an installer of JSignPDF with CloudFoxy integration from 
[JSignPDF @ SourceForge](https://sourceforge.net/projects/jsignpdf/) as CloudFoxy
is [supported from JSignPDF v1.6.4](https://sourceforge.net/projects/jsignpdf/files/stable/JSignPdf%201.6.4/). 


## End of the project ... beginning of use

While there are more notes at the top of this page, JSignPdf has also a detailed 
manual, wich you can read at
[JSignPdf documentation](http://jsignpdf.sourceforge.net/uploads/JSignPdf.pdf)

And don't forget you can send any questions directly to
support@smartarchitects.co.uk we reply within 24 hours.

